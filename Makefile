# SHELL := /bin/bash

# print help
help: ## list available targets (this page)
	 @awk 'BEGIN {FS = ":.*?## "} /^[0-9a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

reveal: install run  ## reveal-md presentation

run:
	@/home/ikar/ctvrtkon/node_modules/.bin/reveal-md \
		--host 0.0.0.0 \
		--watch \
		--theme white \
		--port 2019 \
		README.md

install: .yarn.success  ## install all deps from package.json
	@echo "All deps satisfied!"

pdf: ctvrtkon.pdf
	@echo "Done ✓"

ctvrtkon.pdf: README.md
	@/home/ikar/ctvrtkon/node_modules/.bin/reveal-md \
		--theme white \
		--print ctvrtkon.pdf \
		README.md

.yarn.success: package.json
	@yarn install && touch .yarn.success

clean:  ## clean all the rubbish
	rm -rf ./node_modules
	rm yarn.lock .yarn.success

rndtest:
	echo $$RANDOM

A=makefile_a
B=makefile_b
test:
	@A=shell_a; echo $A
	@B=shell_b; echo $$B
