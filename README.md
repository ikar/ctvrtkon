# makefile
- co to je?
- proč používat makefile?
- jak ho používáme?
- (na co si dát pozor?)

---
# co to je?
soubor, který:
- určuje postup utility make při překladu a
- definuje závislosti mezi zdrojovými soubory

```makefile
CC=gcc
CFLAGS=-I.
DEPS = hellomake.h

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

hellomake: hellomake.o hellofunc.o
	$(CC) -o hellomake hellomake.o hellofunc.o
```
Notes:
* "zdrojak"
* je human-readable;)
* co je to make?

---
# co to je make?
https://cs.wikipedia.org/wiki/Make

Program make je utilita pro automatizaci překladu zdrojových kódů do binárních souborů

Notes:
* puvodni ucel
* nastroj napsany v roce 1976 (prvni DOS 1979)
* snad na vsech unixech
* nepotrebujem nic prekladat abychom nasli vyuziti

---
# proč makefile?
- protože jsme líní

Notes:
* ruku na srdce pratele, kazdy vi, ze lenost je motor pokroku;)

---
# syntaxe
```makefile
target1: dependency1 [...]
	command1
	command2
	...

target2: ...
```
Notes:
- prikazy jsou odsazeny tabulatorem

---
<img src="./spaces-in-makefile.jpeg" width=90%>

Notes:
* Boromir

---
# hello world
```makefile
hello:
	echo "Hello world!"
```
...
```bash
$ make hello
echo "Hello world!"
Hello world!
```

Notes:
* nema dependency
* bash completion
* staci make (prvni cil)
* proc jako?

---
# tato prezentace
```makefile
reveal:  ## reveal-md presentation
	@/home/ikar/ctvrtkon/node_modules/.bin/reveal-md \
		--host 0.0.0.0 \
		--watch \
		--theme white \
		--port 2019 \
		README.md
```
...
```bash
$ make reveal
Reveal-server started at http://0.0.0.0:2019
```
Notes:
* @ - zavinac zabrani vypisu prikazu
* uz nikdy si nemusim pamatovat, kam se instaluji yarn binarky;)

---
# reálný projekt
<img src="./pegasus-make.png" width=90%>

Notes:
- venv, yarn
- pribudou ansibly pro deploy

---
# barevná nápověda
```makefile
help: ## list available targets (this page)
	 @awk 'BEGIN {FS = ":.*?## "} /^[0-9a-zA-Z_-]+:.*?## / \
		 {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' \
		 $(MAKEFILE_LIST)

reveal:  ## reveal-md presentation
	...
```
...

<img src="./make-help.png" width=90%>

---
# barevná nápověda

<img src="./makefile-color-help-target.png" width=90%>

---
# proč makefile?
- protože jsme líní ✓
- protože chceme ušetřit čas

Notes:
- v definici: definuje závislosti mezi zdrojovými soubory

---
<img src="./dependencies.jpg" width=90%>

---
# syntaxe
```makefile
target1: dependency1 [...]
	command1
	command2
	...

dependency1: dependency2
	...

dependency2: ...
	commmand3
```

Notes:
- dependency i target mohou byt soubory
- potom je podminkou provedeni prikazu targetu novejsi dependency nez target

---
# export do pdf

```makefile
pdf:
	@/home/ikar/ctvrtkon/node_modules/.bin/reveal-md \
		--print ctvrtkon.pdf \
		README.md
```
...
```bash
$ time make pdf
Attempting to print "README.md?print-pdf"
to filename "ctvrtkon.pdf" as PDF.

real    0m2,089s
user    0m1,287s
sys     0m0,191s
```

---
# export do pdf
```makefile
pdf: ctvrtkon.pdf
	@echo "Done ✓"

ctvrtkon.pdf: README.md
	@/home/ikar/ctvrtkon/node_modules/.bin/reveal-md \
		--print ctvrtkon.pdf \
		README.md
```
...
```bash
$ time make pdf
Done ✓

real    0m0,007s
user    0m0,004s
sys     0m0,003s
```
Notes:
- 300-nasobne zrychleni

---
# npm / yarn install

```makefile
install: .yarn.success
	@echo "All deps satisfied!"

.yarn.success: package.json
	@yarn install && touch .yarn.success
```

---
# npm / yarn install
```bash
$ time make install
yarn install v1.12.3
[1/4] Resolving packages...
success Already up-to-date.
Done in 0.28s.
All deps satisfied!

real    0m0,723s
user    0m0,770s
sys     0m0,050s
```
```bash
$ time make install
All deps satisfied!

real    0m0,007s
user    0m0,007s
sys     0m0,000s
```
Notes:
* 100-nasobne zrychleni instalace
* lze pouzit s: yarn, npm, compose, pip, ...

---
# tato prezentace
```makefile
reveal: install run  ## reveal-md presentation

run:
	@/home/ikar/ctvrtkon/node_modules/.bin/reveal-md \
		--host 0.0.0.0 --watch --theme white --port 2019 \
		README.md

install: .yarn.success
	@echo "All deps satisfied!"

...
```
...
```bash
$ make reveal
All deps satisfied!
Reveal-server started at http://0.0.0.0:2019
```
----
# děkuji za pozornost

https://gitlab.com/ikar/ctvrtkon

---
# Gotchas
<img src="./live-dangerously.jpg">

---
# Spaces
```makefile
test:
   echo ahoj
```
...
```bash
$ make test
Makefile:33: *** missing separator.  Stop.
```
Notes:
* ani sudy pocet mezer nas nezachrani, musi to byt taby

---
# escapování shell variables
```makefile
A=makefile_a
B=makefile_b
test:
	@A=shell_a; echo $A
	@B=shell_b; echo $$B
```
...
```bash
$ make test
makefile_a
shell_b
```

---
# nejsme v bashi
```makefile
test:
	echo $$RANDOM
```
...
```bash
$ make test
echo $RANDOM

```

tip: http://mywiki.wooledge.org/Bashism

---
# už jsme v bashi
```makefile
SHELL := /bin/bash

test:
	echo $$RANDOM
```
...
```bash
$ make test
echo $RANDOM
42
```

---
# kam dál

http://shop.oreilly.com/product/9780596006105.do

<img src="./amazon-book.jpg" width="30%">


Notes:
- 3. vydani
- 304 stran

---
# děkuji za pozornost

https://gitlab.com/ikar/ctvrtkon
